#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QString>
#include <QTextBrowser>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    //Список подключенных логических дисков (Windows only)
    QFileInfoList filesInfo;
    //Период проверки новых подключенных логических дисков (мс)
    int watcherTime = 1000;
    //Возможно понадобится таймер или создать отдельный класс для этой задачи

    //Выбранный диск для копирования\восстановления
    QString drive;
    //Путь для автоматического сохранения
    QString autoSavePath;
    //Путь для ручного сохранения
    QString manuallySavePath;
    //Путь к резервной копии
    QString backupPath;
    //Список накопителей
    QFileInfoList list;

private slots:
    void update();
    void setDrive(QString);
    void on_toolButton_clicked();
    void on_toolButton_2_clicked();
    void on_commandLinkButton_clicked();
    void copy(QString strFrom, QString strTo, QTextBrowser& textBrowser);
    void on_commandLinkButton_2_clicked();
    void on_lineEdit_editingFinished();
    void on_lineEdit_2_editingFinished();
    void on_toolButton_3_clicked();
    void on_lineEdit_3_editingFinished();
};

#endif // MAINWINDOW_H
