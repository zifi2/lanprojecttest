#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QtDebug>
#include <QFlags>
#include <QDirIterator>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    update();

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(watcherTime);

    connect(ui->comboBox,SIGNAL(currentIndexChanged(QString)), this, SLOT(setDrive(QString)));
    connect(ui->comboBox_2,SIGNAL(currentIndexChanged(QString)), this, SLOT(setDrive(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update(){
    qDebug("Check drive");

    if( list.isEmpty() ) {
        list = QDir::drives();
        foreach( QFileInfo drive, list )
        {
            ui->comboBox->addItem(drive.filePath());
            ui->comboBox_2->addItem(drive.filePath());
            setDrive(ui->comboBox->currentText());
        }
    }
    else {
            foreach( QFileInfo drive, QDir::drives () ){
                if( !list.contains(drive) ){
                    list.append(drive);
                    ui->textBrowser_2->append(tr("Обнаружен новый накопитель ") + drive.filePath());
                    ui->textBrowser_2->append(tr("Начало автоматического копирования"));
                    ui->comboBox->addItem(drive.filePath());
                    ui->comboBox_2->addItem(drive.filePath());
                    copy(drive.canonicalPath(),autoSavePath,*ui->textBrowser_2);
                }
            }
    }

    foreach( QFileInfo drive, list )
    {
        if(!QDir::drives().contains(drive)) {
            ui->comboBox->removeItem( ui->comboBox->findText(drive.filePath()));
            ui->comboBox_2->removeItem( ui->comboBox->findText(drive.filePath()));
            list.removeOne(drive);
        }
    }
}

void MainWindow::setDrive(QString path){
    qDebug() << "setDrive" << path;
    QFileInfo drive(path);
    if(!drive.isRoot()) qDebug() << "debug: selected combobox item is not root directory!";
    else this->drive=drive.filePath();
}

void MainWindow::copy(QString strFrom, QString strTo, QTextBrowser& textBrowser){
    if(QDir(strFrom).path() != "." ){
        qDebug() << QDir(strFrom).path();
         QDirIterator it(strFrom, QDirIterator::Subdirectories);
         while (it.hasNext()) {
             //todo:обернуть try catch
             qDebug() << "copy "<< it.next() << " from " << it.fileInfo().filePath()  << " to ";
             QString testpath = it.fileInfo().filePath();
             qDebug() << testpath.replace(strFrom,strTo+QDir::separator());
             if(it.fileInfo().isDir()) {
                 QString path = it.fileInfo().canonicalFilePath().replace(strFrom,strTo+QDir::separator());
                 QDir dir;
                 dir.mkpath(path);
             }
             else if(it.fileInfo().isFile()) {
                 //todo:если файл существует, то предложить заменить его
                 //не забыть про галочку для таких файлов
                 QFile::copy(it.fileInfo().canonicalFilePath(),
                             it.fileInfo().canonicalFilePath().replace(strFrom,strTo+QDir::separator()));
             }
             textBrowser.append(tr("Копирование объекта ")+ it.fileInfo().canonicalFilePath()+ tr(" завершено"));
             textBrowser.repaint();
         }
         textBrowser.append(tr("Копирование успешно завершено!"));
    }
    else textBrowser.append(tr("Конечной директории не существует"));
}

void MainWindow::on_toolButton_clicked()
{
     autoSavePath = QFileDialog::getExistingDirectory(this,
                                                       tr("Выберите директорию для автосохранения"),
                                                       "C:/",
                                                       QFileDialog::ShowDirsOnly
                                                       | QFileDialog::DontResolveSymlinks);
     qDebug() << autoSavePath;
     ui->lineEdit->setText(autoSavePath);
}

void MainWindow::on_toolButton_2_clicked()
{
    manuallySavePath = QFileDialog::getExistingDirectory(this,
                                                      tr("Выберите директорию для ручного сохранения"),
                                                      "C:/",
                                                      QFileDialog::ShowDirsOnly
                                                      | QFileDialog::DontResolveSymlinks);
    qDebug() << manuallySavePath;
    ui->lineEdit_2->setText(manuallySavePath);
}

void MainWindow::on_toolButton_3_clicked()
{
    backupPath = QFileDialog::getExistingDirectory(this,
                                                      tr("Выберите директорию с резервной копией"),
                                                      "C:/",
                                                      QFileDialog::ShowDirsOnly
                                                      | QFileDialog::DontResolveSymlinks);
    qDebug() << backupPath;
    ui->lineEdit_3->setText(backupPath);
}

void MainWindow::on_commandLinkButton_clicked()
{
    copy(drive,manuallySavePath,*ui->textBrowser_2);
}

void MainWindow::on_commandLinkButton_2_clicked()
{
   copy(backupPath,drive,*ui->textBrowser_3);
}

void MainWindow::on_lineEdit_editingFinished()
{
    if(QDir(ui->lineEdit->text()).exists()) autoSavePath=ui->lineEdit->text();
    else ui->lineEdit->setText(tr("Не могу получить доступ к указанной директории"));
}

void MainWindow::on_lineEdit_2_editingFinished()
{
    if(QDir(ui->lineEdit_2->text()).exists()) manuallySavePath=ui->lineEdit_2->text();
    else ui->lineEdit_2->setText(tr("Не могу получить доступ к указанной директории"));
}



void MainWindow::on_lineEdit_3_editingFinished()
{
    if(QDir(ui->lineEdit_3->text()).exists()) backupPath=ui->lineEdit_3->text();
    else ui->lineEdit_3->setText(tr("Не могу получить доступ к указанной директории"));
}
