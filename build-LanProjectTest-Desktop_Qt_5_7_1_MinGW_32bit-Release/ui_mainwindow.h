/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *copyTab;
    QTabWidget *tabWidget_2;
    QWidget *tab_3;
    QLabel *label;
    QLineEdit *lineEdit;
    QToolButton *toolButton;
    QWidget *tab_4;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineEdit_2;
    QToolButton *toolButton_2;
    QComboBox *comboBox;
    QCommandLinkButton *commandLinkButton;
    QTextBrowser *textBrowser_2;
    QWidget *recoveryTab;
    QCommandLinkButton *commandLinkButton_2;
    QToolButton *toolButton_3;
    QLabel *label_4;
    QLineEdit *lineEdit_3;
    QLabel *label_5;
    QTextBrowser *textBrowser_3;
    QComboBox *comboBox_2;
    QWidget *SettingsTab;
    QGroupBox *groupBox;
    QRadioButton *radioButton;
    QLabel *label_8;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QGroupBox *groupBox_2;
    QLabel *label_9;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QRadioButton *radioButton_7;
    QGroupBox *groupBox_3;
    QLabel *label_6;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_7;
    QCheckBox *checkBox;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(400, 474);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(40);
        sizePolicy.setVerticalStretch(47);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(400, 474));
        MainWindow->setMaximumSize(QSize(400, 474));
        QIcon icon;
        icon.addFile(QStringLiteral(":/img/CopyTab.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MainWindow->setTabShape(QTabWidget::Rounded);
        MainWindow->setDockOptions(QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(10, 10, 371, 441));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabBarAutoHide(false);
        copyTab = new QWidget();
        copyTab->setObjectName(QStringLiteral("copyTab"));
        copyTab->setAutoFillBackground(true);
        tabWidget_2 = new QTabWidget(copyTab);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tabWidget_2->setGeometry(QRect(0, 10, 361, 141));
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        label = new QLabel(tab_3);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 331, 41));
        lineEdit = new QLineEdit(tab_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(10, 60, 301, 20));
        toolButton = new QToolButton(tab_3);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setGeometry(QRect(314, 60, 31, 20));
        tabWidget_2->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        label_2 = new QLabel(tab_4);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 10, 331, 41));
        label_3 = new QLabel(tab_4);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 50, 151, 16));
        lineEdit_2 = new QLineEdit(tab_4);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(10, 70, 301, 20));
        toolButton_2 = new QToolButton(tab_4);
        toolButton_2->setObjectName(QStringLiteral("toolButton_2"));
        toolButton_2->setGeometry(QRect(314, 70, 31, 20));
        comboBox = new QComboBox(tab_4);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(160, 20, 61, 22));
        tabWidget_2->addTab(tab_4, QString());
        commandLinkButton = new QCommandLinkButton(copyTab);
        commandLinkButton->setObjectName(QStringLiteral("commandLinkButton"));
        commandLinkButton->setGeometry(QRect(10, 360, 341, 41));
        commandLinkButton->setCheckable(false);
        textBrowser_2 = new QTextBrowser(copyTab);
        textBrowser_2->setObjectName(QStringLiteral("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(10, 160, 341, 191));
        tabWidget->addTab(copyTab, icon, QString());
        recoveryTab = new QWidget();
        recoveryTab->setObjectName(QStringLiteral("recoveryTab"));
        commandLinkButton_2 = new QCommandLinkButton(recoveryTab);
        commandLinkButton_2->setObjectName(QStringLiteral("commandLinkButton_2"));
        commandLinkButton_2->setGeometry(QRect(10, 360, 341, 41));
        commandLinkButton_2->setCheckable(false);
        toolButton_3 = new QToolButton(recoveryTab);
        toolButton_3->setObjectName(QStringLiteral("toolButton_3"));
        toolButton_3->setGeometry(QRect(314, 70, 31, 20));
        label_4 = new QLabel(recoveryTab);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 10, 331, 41));
        lineEdit_3 = new QLineEdit(recoveryTab);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(10, 70, 301, 20));
        label_5 = new QLabel(recoveryTab);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 50, 151, 16));
        textBrowser_3 = new QTextBrowser(recoveryTab);
        textBrowser_3->setObjectName(QStringLiteral("textBrowser_3"));
        textBrowser_3->setGeometry(QRect(10, 110, 341, 231));
        comboBox_2 = new QComboBox(recoveryTab);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(220, 20, 61, 22));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/img/RecoveryTab.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(recoveryTab, icon1, QString());
        SettingsTab = new QWidget();
        SettingsTab->setObjectName(QStringLiteral("SettingsTab"));
        groupBox = new QGroupBox(SettingsTab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 130, 341, 111));
        radioButton = new QRadioButton(groupBox);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(10, 40, 101, 17));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 10, 331, 31));
        radioButton_2 = new QRadioButton(groupBox);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(10, 60, 311, 17));
        radioButton_3 = new QRadioButton(groupBox);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setGeometry(QRect(10, 80, 311, 17));
        groupBox_2 = new QGroupBox(SettingsTab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 260, 341, 141));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 20, 331, 31));
        radioButton_4 = new QRadioButton(groupBox_2);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setGeometry(QRect(10, 50, 271, 17));
        radioButton_5 = new QRadioButton(groupBox_2);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setGeometry(QRect(10, 70, 271, 17));
        radioButton_6 = new QRadioButton(groupBox_2);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));
        radioButton_6->setGeometry(QRect(10, 90, 271, 17));
        radioButton_7 = new QRadioButton(groupBox_2);
        radioButton_7->setObjectName(QStringLiteral("radioButton_7"));
        radioButton_7->setGeometry(QRect(10, 110, 271, 17));
        groupBox_3 = new QGroupBox(SettingsTab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 20, 341, 101));
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 30, 331, 61));
        doubleSpinBox = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(60, 60, 62, 22));
        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(130, 40, 331, 61));
        checkBox = new QCheckBox(groupBox_3);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(10, 20, 171, 17));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/img/SettingsTab.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(SettingsTab, icon2, QString());
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        MainWindow->setMenuBar(menuBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(2);
        tabWidget_2->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\242\320\265\321\201\321\202\320\276\320\262\320\276\320\265 \320\267\320\260\320\264\320\260\320\275\320\270\320\265", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">\320\237\321\200\320\270 \320\277\320\276\320\264\320\272\320\273\321\216\321\207\320\265\320\275\320\270\320\270 \320\275\320\260\320\272\320\276\320\277\320\270\321\202\320\265\320\273\321\217 \320\260\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270</p><p>\320\241\320\276\321\205\321\200\320\260\320\275\321\217\321\202\321\214 \320\276\320\261\321\200\320\260\320\267 \320\275\320\260\320\272\320\276\320\277\320\270\321\202\320\265\320\273\321\217 \320\262 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\321\216</p></body></html>", Q_NULLPTR));
        lineEdit->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\277\321\203\321\202\321\214 \320\264\320\276 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\320\270 \321\200\320\265\320\267\320\265\321\200\320\262\320\270\321\200\320\276\320\262\320\260\320\275\320\270\321\217", Q_NULLPTR));
        toolButton->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_3), QApplication::translate("MainWindow", "\320\220\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\276\320\265 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "\320\241\320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\275\320\260\320\272\320\276\320\277\320\270\321\202\320\265\320\273\321\214", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "\320\222 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\321\216", Q_NULLPTR));
        lineEdit_2->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\277\321\203\321\202\321\214 \320\264\320\276 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\320\270 \321\201\320\276\321\205\321\200\320\260\320\275\320\265\320\275\320\270\321\217", Q_NULLPTR));
        toolButton_2->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_4), QApplication::translate("MainWindow", "\320\240\321\203\321\207\320\275\320\276\320\265 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", Q_NULLPTR));
        commandLinkButton->setText(QApplication::translate("MainWindow", "\320\235\320\260\321\207\320\260\321\202\321\214 \321\200\321\203\321\207\320\275\320\276\320\265 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", Q_NULLPTR));
        textBrowser_2->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\320\227\320\264\320\265\321\201\321\214 \320\261\321\203\320\264\320\265\321\202 \320\270\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217 \320\276 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\270</p></body></html>", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(copyTab), QApplication::translate("MainWindow", "\320\232\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", Q_NULLPTR));
        commandLinkButton_2->setText(QApplication::translate("MainWindow", "\320\235\320\260\321\207\320\260\321\202\321\214 \320\262\320\276\321\201\321\201\321\202\320\260\320\275\320\276\320\262\320\273\320\265\320\275\320\270\320\265", Q_NULLPTR));
        toolButton_3->setText(QApplication::translate("MainWindow", "...", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "\320\222\320\276\321\201\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214 \321\201\320\276\320\264\320\265\321\200\320\266\320\270\320\274\320\276\320\265 \320\275\320\260\320\272\320\276\320\277\320\270\321\202\320\265\320\273\321\217", Q_NULLPTR));
        lineEdit_3->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\277\321\203\321\202\321\214 \320\264\320\276 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\320\270 \321\201 \321\200\320\265\320\267\320\265\321\200\320\262\320\275\320\276\320\271 \320\272\320\276\320\277\320\270\320\265\320\271", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "\320\230\320\267 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\320\270", Q_NULLPTR));
        textBrowser_3->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\320\227\320\264\320\265\321\201\321\214 \320\261\321\203\320\264\320\265\321\202 \320\270\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217 \320\276 \320\262\320\276\321\201\321\201\321\202\320\260\320\275\320\276\320\262\320\273\320\265\320\275\320\270\320\270</p></body></html>", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(recoveryTab), QApplication::translate("MainWindow", "\320\222\320\276\321\201\321\201\321\202\320\260\320\275\320\276\320\262\320\273\320\265\320\275\320\270\320\265", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "\320\241\320\270\320\274\320\262\320\276\320\273\321\214\320\275\321\213\320\265 \321\201\321\201\321\213\320\273\320\272\320\270", Q_NULLPTR));
        radioButton->setText(QApplication::translate("MainWindow", "\320\230\320\263\320\275\320\276\321\200\320\270\321\200\320\276\320\262\320\260\321\202\321\214", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\237\321\200\320\270 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\270 \321\201\320\270\320\274\320\262\320\276\320\273\321\214\320\275\321\213\320\265 \321\201\321\201\321\213\320\273\320\272\320\270</p></body></html>", Q_NULLPTR));
        radioButton_2->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\277\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\260\320\261\321\201\320\276\320\273\321\216\321\202\320\275\321\213\320\271 \320\277\321\203\321\202\321\214 \321\201\321\201\321\213\320\273\320\272\320\270", Q_NULLPTR));
        radioButton_3->setText(QApplication::translate("MainWindow", "\320\230\320\267\320\274\320\265\320\275\321\217\321\202\321\214 \320\277\321\203\321\202\321\214 \320\275\320\260 \320\276\321\202\320\275\320\276\321\201\320\270\321\202\320\265\320\273\321\214\320\275\321\213\320\271", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "\320\224\320\265\320\271\321\201\321\202\320\262\320\270\320\265 \321\201 \320\272\320\276\320\277\320\270\320\265\320\271", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\225\321\201\320\273\320\270 \321\203\320\266\320\265 \321\201\321\203\321\211\320\265\321\201\321\202\320\262\321\203\320\265\321\202 \321\204\320\260\320\271\320\273 \321\201 \320\270\320\274\320\265\320\275\320\265\320\274 \320\272\320\276\320\277\320\270\321\200\321\203\320\265\320\274\320\276\320\263\320\276 \321\204\320\260\320\271\320\273\320\260</p></body></html>", Q_NULLPTR));
        radioButton_4->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\274\320\265\320\275\320\270\321\202\321\214 \321\201\321\203\321\211\320\265\321\201\321\202\320\262\321\203\321\216\321\211\320\270\320\271 \321\204\320\260\320\271\320\273", Q_NULLPTR));
        radioButton_5->setText(QApplication::translate("MainWindow", "\320\237\320\265\321\200\320\265\320\270\320\274\320\265\320\275\320\276\320\262\320\260\321\202\321\214 \321\201\321\203\321\211\320\265\321\201\321\202\320\262\321\203\321\216\321\211\320\270\320\271 \321\204\320\260\320\271\320\273", Q_NULLPTR));
        radioButton_6->setText(QApplication::translate("MainWindow", "\320\237\320\265\321\200\320\265\320\270\320\274\320\265\320\275\320\276\320\262\320\260\321\202\321\214 \320\272\320\276\320\277\320\270\321\200\321\203\320\265\320\274\321\213\320\271 \321\204\320\260\320\271\320\273", Q_NULLPTR));
        radioButton_7->setText(QApplication::translate("MainWindow", "\320\235\320\265 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \321\204\320\260\320\271\320\273", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\320\220\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\276\320\265 \320\272\320\276\320\277\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\320\237\321\200\320\276\320\262\320\265\321\200\321\217\321\202\321\214 \320\277\320\276\320\264\320\272\320\273\321\216\321\207\320\265\320\275\320\270\320\265 \320\275\320\276\320\262\321\213\321\205 \320\275\320\260\320\272\320\276\320\277\320\270\321\202\320\265\320\273\320\265\320\271</p><p>\320\272\320\260\320\266\320\264\321\213\320\265</p></body></html>", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\321\201\320\265\320\272\321\203\320\275\320\264</p></body></html>", Q_NULLPTR));
        checkBox->setText(QApplication::translate("MainWindow", "\320\222\320\272\320\273\321\216\321\207\320\265\320\275\320\276", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(SettingsTab), QApplication::translate("MainWindow", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
